package com.ea.utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.Keys as Keys


public class CustomKey {

	//#############################
	//XPATH CREATOR :
	//#############################

	static def XpathCreate(def elementType, def elementLabel) {
		switch(elementType){

			//*=========  BUTTONS AND BOXES ===============
			case 'buttons':
				def newpath = "//*[@type = 'submit' and (text() = ' "+ elementLabel +" ' or . = ' "+ elementLabel +" ')]";
				return newpath;
				break;
			case 'button': // button
				def newpath = "//button[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'backButton': //forget password backbutton
				def newpath = "//button[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'radioButton'://Checbox or dropdown labels
				def newpath = "//label[@class = 'mt-radio mt-radio-outline' and (text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'textBox': // ANY Text box
				def newpath = "//input[@class = 'form-control placeholder-no-fix' and @placeholder = '"+ elementLabel +"']";
			//def newpath = "//input[@id = '" + elementLabel + "']";
				return newpath;
				break;
			case 'textBox_noName': // ANY Text box
				def newpath = "//input[@id = '"+ elementLabel +"' and @class = 'form-control']";
				return newpath;
				break;
			case 'searchBox': //Search Box
				def newpath = "//input[@class = 'form-control' and @placeholder = '"+ elementLabel +"']";
			//def newpath = "//input[@id = '" + elementLabel + "']";
				return newpath;
				break;
			case 'optionBox': //text box when clicking dropdown option
				def newpath = "//input[@class = 'select2-search__field' and @type = 'search' and @role = 'textbox']";
				return newpath;
				break;
			case 'conboBox': //text box when clicking dropdown option
				def newpath = "//span[@role = 'combobox' and (text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;

			//=========== MENU ITEMS ============
			case 'menuItem'://Menu Item and Sub item
			//def newpath = "//a[@class = 'nav-link nav-toggle' and (text() = '  "+ elementLabel +"  ' or . = '  "+ elementLabel +"  ')]";
				def newpath = "//span[@class = 'title' and (text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'tabTitle'://Title when you open a Tab or page from Menu
				def newpath = "//h1[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;


			//============ LABELS AND TEXT ================
			case 'labels'://Checbox or dropdown labels
				def newpath = "//label[(text() = '"+ elementLabel +" ' or . = '"+ elementLabel +" ')]";
				return newpath;
				break;
			case 'textItem'://Plain Text Items
				def newpath = "//span[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'anyText'://Any Text Items
				def newpath = "//*[contains(text(), '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'boldLabel'://Labels that are bold
				def newpath = "//strong[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
			case 'listItem':
				def newpath = "//li[@class = 'ms-elem-selectable ms-hover' and (text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
				break;
			case 'aTag': // Link text
				def newpath = "//a[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'h3Tag':
				def newpath = "//h3[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'h4Tag': // Page sub title tag
				def newpath = "//h4[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'pTag':
				def newpath = "//p[(text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
			case 'logoSg_UAT':
				def newpath = "//img[@src = '/bundles/admin/assets/pages/img/logo.png?109697']";
				return newpath;
				break;
			case 'tryItem':
				def newpath = "//li[@class = 'ms-elem-selectable ms-hover' and (text() = '"+ elementLabel +"' or . = '"+ elementLabel +"')]";
				return newpath;
				break;
		}

	}

	@Keyword
	def itemXpath(def element, def label) {
		//TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//*[@type = 'submit' and (text() = ' Login ' or . = ' Login ')]")
		def xpathnew = XpathCreate(element, label);
		//print xpathnew
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		return Element;
	}

	@Keyword
	def openURL(def Url) {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(Url)
		WebUI.delay(1)
	}

	@Keyword
	def navigateMenu(def menu) {
		def xpathnew = XpathCreate('menuItem', menu);
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		WebUI.click(Element)
	}

	@Keyword
	def navigateSubMenu(def menu, def submenu) {
		def xpathnew = XpathCreate('menuItem', menu);
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		WebUI.click(Element)
		WebUI.delay(1)
		def xpathnew2 = XpathCreate('menuItem', submenu);
		TestObject Element2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew2)
		WebUI.click(Element2)
	}

	@Keyword
	def selectDropdown(def boxname, def info) {
		def xpathnew = XpathCreate('conboBox', boxname);
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		WebUI.click(Element)
		WebUI.delay(1)
		def xpathnew2 = XpathCreate('optionBox', ' ');
		TestObject Element2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew2)
		//WebUI.click(Element2)
		WebUI.setText(Element2, info)
		WebUI.sendKeys(Element2, Keys.chord(Keys.ENTER))
	}






	@Keyword
	def ClickElement(def element, def label) {
		//TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//*[@type = 'submit' and (text() = ' Login ' or . = ' Login ')]")
		def xpathnew = XpathCreate(element, label);
		//print xpathnew
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		WebUI.click(Element)
	}

	@Keyword
	def SendText(def element, def label, def content) {
		//WebUI.setText(findTestObject('Object Repository/gmedes/login/username_textbox'), userName)
		def xpathnew = XpathCreate(element, label);
		//print xpathnew
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		WebUI.setText(Element, content)
	}

	@Keyword
	def loginTo(def url, def userName, def password) {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(url)
		WebUI.setText(findTestObject('Object Repository/gmedes/login/username_textbox'), userName)
		WebUI.setText(findTestObject('Object Repository/gmedes/login/password_textbox'), password)
		WebUI.click(findTestObject('Object Repository/gmedes/login/login_button'))
	}

	@Keyword
	def logIn() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.URL)
		WebUI.setText(findTestObject('Object Repository/gmedes/login/username_textbox'), GlobalVariable.USERNAME)
		WebUI.setText(findTestObject('Object Repository/gmedes/login/password_textbox'), GlobalVariable.PASSWORD)
		WebUI.click(findTestObject('Object Repository/gmedes/login/login_button'))
		WebUI.delay(3)
	}
	//#############################
	//VERIFY KEYWORDS :
	//#############################


	@Keyword
	def CheckDropdownList(TestObject object, String option) {
		boolean flag = false;
		WebElement element = WebUiCommonHelper.findWebElement(object, 20);
		Select ddl = new Select(element);
		for(WebElement elem : ddl.getOptions()){

			if(elem.getText().equals(option)){
				System.out.println("Element Exist");
				return flag = true;
			}
			else {
				System.out.println("Element Not Exist");
			}
		}
		return flag;
	}

	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	@Keyword
	def TryClick() {
		TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//a[@class = 'nav-link nav-toggle' and (text() = '  Agent  ' or . = '  Agent  ')]")
		//TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//a[contains(@text='Agent')]")
		//def xpathnew = XpathCreate(element, label);
		//print xpathnew
		//TestObject Element = new TestObject().addProperty('xpath', ConditionType.EQUALS, xpathnew)
		WebUI.click(Element)
	}

	//############################# END ########################################







	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUiBuiltInKeywords.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}




}