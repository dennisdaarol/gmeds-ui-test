import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'com.ea.utilities.CustomKey.navigateSubMenu'('Patients', 'List Patients')
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('tabTitle', 'List Patients'))
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('searchBox', 'Search by Patient Name/Code'), findTestData(
        'patientsData').getValue('patientsName', 1))
WebUI.sendKeys(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('searchBox', 'Search by Patient Name/Code'), Keys.chord(Keys.ENTER))
WebUI.delay(1)
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('anyText', 'Singapatient'))
WebUI.delay(1)
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('searchBox', 'Search by Patient Name/Code'), findTestData(
	'patientsData').getValue('patientsName', 2))
WebUI.sendKeys(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('searchBox', 'Search by Patient Name/Code'), Keys.chord(Keys.ENTER))
WebUI.delay(1)
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('anyText', 'Indopatient'))

//Go back to dashboard
CustomKeywords.'com.ea.utilities.CustomKey.navigateMenu'('Dashboard')
