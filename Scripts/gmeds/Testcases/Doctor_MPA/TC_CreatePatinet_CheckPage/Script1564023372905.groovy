import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'com.ea.utilities.CustomKey.navigateSubMenu'('Patients', 'Register New Patient')
WebUI.delay(1)
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('tabTitle', 'Register Patient'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('h4Tag', 'Patient Information'))

//Confirm checkboxes
WebUI.verifyElementVisible(findTestObject('Doctor_MPA/Patients/Confirm_Checkbox1')) //Confirm Checbox 1
WebUI.verifyElementVisible(findTestObject('Doctor_MPA/Patients/Confirm_Checkbox2')) //Confirm Checkbox 2

//Check All labels
for (def index : (1..9)) {
	WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('labels', findTestData('patientsData').getValue('CreatePatientPage', index)))
}

//Geneder radio button
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', 'Male\n\n'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', 'Female\n\n'))

//Caregiver Info
WebUI.verifyElementVisible(findTestObject('Object Repository/Doctor_MPA/Patients/caregiver_info'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', 'Yes, patient prefers to be contacted through the assigned caregiver / payer\n\n'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', 'No, patient prefers to be contacted directly\n\n'))

//Allergy information
WebUI.verifyElementVisible(findTestObject('Object Repository/Doctor_MPA/Patients/Allegy_info'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('boldLabel', 'Known Medication Allergies *'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', ' Yes\n\n\n'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', ' No\n\n\n'))

//Diagnosis
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('boldLabel', 'Diagnosis *'))

//Buttons
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', ' Cancel '))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', ' Save '))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', ' Save and Create Prescription \n'))

//Go back to dashboard
CustomKeywords.'com.ea.utilities.CustomKey.navigateMenu'('Dashboard')