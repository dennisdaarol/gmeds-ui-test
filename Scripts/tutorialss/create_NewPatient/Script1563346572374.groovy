import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//WebUI.navigateToUrl('https://uat.mygmeds.com/login')
//WebUI.setText(findTestObject('null'), 'gmedestest+04051@gmail.com')
//WebUI.setEncryptedText(findTestObject('null'), 'ca6bNFbO6Y11xW2TS02G/Q==')
//WebUI.click(findTestObject('null'))
//WebUI.click(findTestObject('null'))
//WebUI.click(findTestObject('null'))
//WebUI.click(findTestObject('null'))
//WebUI.click(findTestObject('null'))
//Use custom keyword
//CustomKeywords.'com.ea.utilities.CustomKey.CheckDropdownList'(findTestObject('null'), 'Mrs.')
//WebUI.verifyElementText(findTestObject('null'), 'Mr.Ms.Mrs.Mdm.Dr.Prof.')
//WebUI.setText(findTestObject('record/createPatient/title_enterTitle'), 'Mrs.')
//WebUI.click(findTestObject('null'))
//WebUI.focus(findTestObject('null'))

//################  Select Dropdown List ####################
WebUI.click(findTestObject('null'))
WebUI.delay(1)
WebUI.sendKeys(findTestObject('null'), 'Mrs.')
WebUI.sendKeys(findTestObject('null'), Keys.chord(Keys.ENTER))

